# RDKit Dockerfile

This repository contains a Dockerfile of [RDKit](https://github.com/rdkit/rdkit) for an automated build on [Docker Hub](https://hub.docker.com/r/ssmehta/rdkit/).  RDKit is a collection of cheminformatics and machine-learning software written in C++ and Python.

## Usage

To run a container in interactive mode

    docker run -it ssmehta/rdkit

A specific version of RDKit can be pulled using

    docker run -it ssmehta/rdkit:<version>

The available versions are

* `latest` corresponds to the most recent stable release in this list
* `Release_2016_03_3`
* `Release_2016_03_2`
* `Release_2016_03_1`
* `Release_2015_09_2`
* `Release_2015_03_1`
* `Release_2014_09_2`

It is also possible to mount a host directory or attach data volume to the container's `/data` directory.  The entrypoint script is set up to look for and automatically execute a script at `/data/run.sh` it if it exists.  

## Building

Build with the usual

    docker build -t rdkit .

and then run with

    docker run -it rdkit
