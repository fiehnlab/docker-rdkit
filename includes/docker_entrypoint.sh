#!/bin/bash

if [ -f "/data/run.sh" ]; then
    cd /data/
    bash run.sh
else
    echo "** Runner script /data/run.sh does not exist. Entering bash shell..."
    bash
fi
